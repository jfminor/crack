hashpass: hashpass.o md5.o
	clang hashpass.o md5.o -o hashpass -l ssl -l crypto

md5.o: md5.c md5.h
	clang -g -c md5.c -Wall
    
hashpass.o: hashpass.c md5.h
	clang -g -c hashpass.c -Wall
    
hashes: hashpass
	./hashpass rockyou100.txt hashes.txt
	
check: hashpass
	valgrind ./hashpass rockyou100.txt hashes.txt
	
clean:
	rm -f *.o hashpass hashes.txt